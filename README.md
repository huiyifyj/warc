# WARC

> 📦 `WARC` 目的是将 Web 页面及其资源打包到单个文件中, 最终达到文件归档的目的。

`WARC` 是 go 语言的库, 可将网页及其资源归档打包到单个 [`bolt`](https://github.com/etcd-io/bbolt) 数据库文件中。

处于开发阶段, 该库只是针对我的项目而写的, 可能 API 接口变动会很大, 所以不建议使用。 但是该项目使用的 `bolt` 数据库在 API 和数据库文件方面是很稳定。
**注意**: 目前 `WARC` 在存档页面时会禁用 `JavaScript`, 因此可能它在 [`SPA`](https://baike.baidu.com/item/SPA/17536313) 网站中效果不会太好。

## 安装方法

安装该库只需运行命令行 `go get`:
```shell
go get -u -v gitea.com/huiyifyj/warc
```

## 协议

> `WARC` 库遵守 [MIT](https://opensource.org/licenses/MIT) 协议

这意味你可以随意改动, 只要注明我 [@huiyifyj](https://github.com/huiyifyj) 即可。
