module gitea.com/huiyifyj/warc

require (
	github.com/sirupsen/logrus v1.5.0
	github.com/tdewolff/parse v2.3.4+incompatible
	go.etcd.io/bbolt v1.3.4
)

go 1.14
